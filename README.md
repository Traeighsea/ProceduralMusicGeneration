# Procedural Music Generation

I've split this project in half to implement a general purpose music theory library in another repo [here](https://gitlab.com/Traeighsea/tml). This is my current focus from which this project will be a specific use case or example of how to utilize that library.

## Introduction

This started as a project for my Embedded Systems class with 2 other individuals. I worked primarily on the music generation algorithm and they worked more primarily on some of the embedded systems side of things, reading in and determining colors from the color sensor, the small web server that delivered the midi file via email, and the "duct tape" that held everything together. I've since moved the procedural music generation code into a new repository for further development.

You can see some examples of the output from the output directory [here](https://gitlab.com/Traeighsea/ProceduralMusicGeneration/-/tree/master/output).

## Overview

The algorithm works currently by taking in a seed, which for the purposes of the original project were a collection of predetermined colors that the color sensor could pick up. The color is directly mapped to a number, this number is used for decisions in the process.

I used simple function music theory as the basis for the model of decision making. Functional harmony allows us to simplify the algorithm quite a bit as we can stick to a single key center with only 7 notes. Additionally this allows us to look at chords as performing certain functions within a musical context such that there is a "correct" way to move to a new chord. This gives us a state diagram we can use to transition between chords that sound consonant and make logical sense. 

Usually from a given chord, there is a respective scale that will sound good, and emphasizing notes within that chord are ideal to sound more musical. However, generally speaking playing any note within the key will sound _fine_ on its own even within the randomness. After all repetition legitimizes. Repetition legitimizes. While keeping the notes more or less random and the chords simple 3 chords, we can essentially change the overall harmony with the notes played by the melody which will result in extended chords like adding the 7th, 9th, 13th, etc. via the melody. This will give us variation and make it more harmonically interesting to the listener. The idea here is that provided we stay within the rules of functional harmony, we can ensure the output sounds consonant enough that it doesn't sound procedurally generated.

## The algorithm

The algorithm takes a top down approach to generation. First we start with getting the instruments for the lead and the rhythm, the Beats Per Minute (BPM), and key center. Next we decide the form, which gives us an idea of how the song will be laid out in sections. The general and common structures for the songs I chose were Ternary (ABA') form and Binary (AA'BB') form. Where the A section would be the main topic/theme/verse of the song, and the B section would be the counter topic/theme/chorus with prime versions being a variation of that main theme. These are simple and were fairly common in classical eras and an easy way to output song with some variation. Within each section it is broken down into number of bars per section, with the options being 2 or 4 bars.

After generating the number of bars, we generate the chords. The note length is chosen first for the chord from which each possible color has a different note length distribution such that certain rhythms are prioritized and more likely to happen. If the note length would go over the length of the bar, then it would choose the next note length down. This process would continue until there is a note length that can be chosen. The chord function; I, ii, iii, IV, V, vi, vii, is then chosen based on the state diagram, with certain colors prioritizing certain chord progressions. The idea here is that certain chords will sound better as a next chord, at least generally speaking, so they have higher weights. Next we decide what octave will show up here which will determine how the chord is voiced. The voicing needs to be based on what has happened previously so that chord changes don't sound jarring to the listener, this is accounted for when voicing the chord.

After the chord generation is done we generate the melody. The melody rhythm is chosen first in a similar process to the chords with remaining note lengths. After the note length is chosen the starting note is chosen more or less randomly based on the seed. After the first note, we need to choose our notes more carefully. When choosing this note we need to know what scale or mode to start from, and choose a scale degree; second, third, fourth, fifth, sixth, or seventh, from that starting note. Different numbers or colors from the seed correlate to different scale degrees. After this process completes, the section is finished.

Finally we would continue through the rest of the sections and create whatever other sections are needed. In the case of variations such as A', we would create a section based on the previous A section. This would be as simple as modifying a couple note names of the melody, changing up a couple rhythms, adding notes, removing notes, maybe even just moving up all of the notes a single scale degree. Generally speaking not much needs to change to have a variation of the previous melody. Although I had an idea of how this would work at the time, when I originally finished this project this part was omitted due to time constraints and other projects.

Last step of the entire process is the MIDI output which is performed from the craigsapp midi library (listed in the credits)

## Improvements and the Future of this project

Generally speaking I would love for this project to be a great tool for users to generate ideas from and expand upon on their own.

First thing I see when looking back on this project is how poorly _all_ of the code is written knowing what I know now. To be completely fair this entire project was effectively coded from 8PM-6AM the night before it was due during a coffee binge, driven by killer tunes in an all too familiar, and classically traditional, college past time of an all nighter format. Ya know the formula for success and effective code writing. The algorithm is quality and it works, but overall this isn't good code. I need to clean up a lot of the overall project and some of the design choices, use more object oriented principles, move objects to their own files, remove some of the god awful switch cases that result in hundreds of hard coded values.. Anyways. TDLR; clean up codebase

As far as the algorithm goes, I had so many ideas I wanted to do but just didn't have time for. First thing that comes to mind is using a [Stochastic Process](https://en.wikipedia.org/wiki/Stochastic_process#Stochastic_process) for decision making. There are a few reasons for this. Primarily it could be used to increase the likelihood that the generated music sounds good. Music that follows the rules too tends to sound boring, and music that is random sounds unorganized or bad at best. You need both structure to keep you grounded, and some "randomness" to keep the listener interested. With using a Stochastic Process, we can increase the probability of weights for things that we know from theory will sound good, while stil. having some variation to sound interesting. Additionally this brings me into another feature that would be great to have; configuration files and user involvement.

There are a lot of different types of music. There are also a lot of ways to organize music and analyze using theory. If I can add a way for the user to modify the algorithm themselves and increase weights of certain parameters, we can get closer to great output. With this in mind, allowing the user to add other rules to the music themselves that would be even better. For example, prioritizing using the Phrygian Dominant scale wherever possible, playing a certain lick or melody with a certain frequency within the algorithm, generating weirder or more simple music, the possibilities are limitless here based on my configuration capabilities.

Another feature that would be nice to have would be to render the audio itself instead of only MIDI. I'm not as knowledgable about rendering audio, but that is an area I would love to look more into. In that way it would open up the algorithm to a lot more acoustic variables as well including filters, dynamics, sweeps, fades, etc. that an audio engineer could use.

### List of future improvements:

- Cleanup code
- Remove the colors as input, using a more typical seed
- Add Stochastic Process
- Add User Configuration via JSON files
- Allow for more fine tuning of the algorithm via those JSON files
- Allow for custom rules by the user
- Add capabilities for more music theory models (such as 12 Tone, romantic era style of composing, Aleatoric music, etc.)
- Add rendered audio capability
- Add multiple passthroughs to allow for more fine tuning based on each aspect of the music
- Add more instruments
- Add more complex music theory concepts
- Add different algorithms based on the instrument

## Credits

This program currently uses a library made by Craig Stuart Sapp (craigsapp), that allows MIDI file creation and manipulation with C.
