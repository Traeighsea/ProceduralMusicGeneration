#ifndef __MusicProcessor_H__
#define __MusicProcessor_H__

#include "MidiFile.h"
#include <iostream>
#include <ctime>

//This is used to output generation process
const bool TRACE_DEBUG = true;
// std::fstream LOG_FILE;
//

//*Note: set rhythm and noteName to -1 to mean that the note hasn't been initialized yet
//      and/or means the song is over once it gets to the -1
struct note
{
    int noteName;
    int rhythm;
};

struct chord
{
    int noteRoot;
    int noteThird;
    int noteFifth;
    int rhythm;
};

////////////////////////
//  The following functions are for music theory
////////////////////////

////////////////////////
//  The following functions and structures are for song structure
////////////////////////

class smallSection
{
private:
    int bars;
    note melody[64];
    chord harmony[32];
    note *melodyPtr;
    chord *harmonyPtr;

    void generateChords(int key);
    void generateMelody(int key);

public:
    smallSection();
    ~smallSection();
    //smallSection(const smallSection&);

    void generatorLoop(int key);
    void variationGeneratorLoop(int key);
    note getNote(int n);
    chord getChord(int n);
};

class songSection
{
private:
    //Form can be:
    //  Binary = 0, which follows the form aa'bb'
    //  Ternary = 1, which follows the form aba'
    //  *form should not be changed after creation
    int smallForm;
    int sectionNum; //the number of sections

    smallSection sections[4];
    colorContainer *colorList;

    void createSection(int sectionNumber, int key);
    void createVariationFromSection(int sectionNumber, int variationNumber, int key);

public:
    songSection();
    ~songSection();
    //SongSection(const SongSection&);

    void generatorLoop(int key, colorContainer *inColorList);
    void variationGeneratorLoop(int key, colorContainer *inColorList);
    int getSectionNum();
    smallSection *getSmallSection(int n);
};

class MidiFileGenerator
{
private:
    int melodyInstrument;
    int harmonyInstrument;
    int bpm;
    //Form can be:
    //  Binary = 0, which follows the form AA'BB'
    //  Ternary = 1, which follows the form ABA'
    //  *form should not be changed after creation
    int largeForm;
    int key;

    int sectionNum; //the number of sections
    songSection sections[4];
    colorContainer colorList;

    void createSection(int sectionNumber);
    void createVariationFromSection(int sectionNumber, int variationNumber);

public:
    MidiFileGenerator();
    ~MidiFileGenerator();

    void mainGeneratorLoop();
    void outputToFile();
    void writeMSPFile();
    void setColorList(colorType *);
};

#endif // __MusicProcessor_H__
