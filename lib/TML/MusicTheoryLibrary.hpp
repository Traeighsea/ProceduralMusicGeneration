#ifndef MUSICTHEORYLIBRARY_HPP
#define MUSICTHEORYLIBRARY_HPP

#include <iostream>
#include <vector>

/// TODO: This is just.. bad. The whole thing.. Gonna need a complete redesign
/// TODO: Determine and clean up naming conventions
/// TODO: Potentially separate this into its' own library to include music theory concepts

/// This file contails a collection of concrete music theory concepts

/// Namespace Traeighsea's Music (Theory) Library
namespace tml
{
   /// Circularly linked list to represent each leter notation
   ///   [Letter Notation](https://en.wikipedia.org/wiki/Letter_notation)
   const int LetterNotation[7] = {0, 2, 4, 5, 7, 9, 11};

   enum PitchName
   {
      C = 0,
      D = 2,
      E = 4,
      F = 5,
      G = 7,
      A = 9,
      B = 11
   };

   /// Wrote out the note names for ease of calling out the names, also kept both forms of short note name and full name
   ///   [Pitch Class](https://en.wikipedia.org/wiki/Pitch_class)
   enum PitchClass
   {
      C = 0,
      Cs = 1,
      Csharp = 1,
      Db = 1,
      Dflat = 1,
      D = 2,
      Ds = 3,
      Dsharp = 3,
      Eb = 3,
      Eflat = 3,
      E = 4,
      Fb = 4,
      Fflat = 4,
      Es = 5,
      Esharp = 5,
      F = 5,
      Fs = 6,
      Fsharp = 6,
      Gb = 6,
      Gflat = 6,
      G = 7,
      Gs = 8,
      Gsharp = 8,
      Ab = 8,
      Aflat = 8,
      A = 9,
      As = 10,
      Asharp = 10,
      Bb = 10,
      Bflat = 10,
      B = 11,
      Cb = 11,
      Cflat = 11,
      Bs = 12,
      Bsharp = 12
   };

   /// TODO:
   ///   [Pitch](https://en.wikipedia.org/wiki/Pitch_(music))
   class Pitch
   {
   public:
      /// TODO: Function to get scientific pitch notation

      /// TODO: Function to get Helmholtz pitch notation

   private:
      /// The letter name for purposes of keeping the music theory in tact (as opposed to the midi int or the pitch class, although pitch classes will be used for calculation most likely). This will make it easier to build melodic lines from if you already know the note name/letter to go from the pitch cycle.
      PitchName pitchName_ = PitchName::C;
      /// For our purposes this is wether the note is flat, sharp, or any other modification from the base pitch. The number represents the number of half steps.
      int accidental_ = 0;
      /// Min of -1, max of 9
      int octave_ = 4;

      // Could potentially use inheritance for these values to overwrite the functions for when we obtain the duration

      /// If you'd rather manipulate the MIDI notes yourself for full control, have fun
      bool usingNoteNumbers_ = false;
      /// Relative to a MIDI note number
      unsigned int noteNumber_ = 0;
   };

   /// NoteValues are relative values or durations as you would use in typical music notation. Since the value is relative to the tempo, and the note does not have knowledge of the tempo at this level, the noteValue needs to be calculated with the tempo to get the duration in ms/s.
   ///   [Note Values](https://en.wikipedia.org/wiki/Note_value)
   enum NoteValue
   {
      Whole = 1,
      Half = 2,
      Quarter = 4,
      Eighth = 8,
      Sixteenth = 16,
      ThirtySecond = 32,
      SixtyFourth = 64,
      HundredTwentyEighth = 128,  // whyyy bro?
      TwoHundredFiftySixth = 256, // BRO STAHP

      // Have to get creative with these guys
      DoubleWhole = -2,
      Longa = -4,
      Maxima = -8 // did you leave the note on bro?
   };

   /// TODO:
   class Duration
   {
   public:
   private:
      /// The noteValue is using relative values as you would
      NoteValue noteValue_ = NoteValue::Whole;
      /// The modifer is the number of dotted values added to the note. Realistically shouldn't really go above 3, but go crazy fam.
      ///   The algorithm for dotted notes is ((2^n - 1) / 2^n)
      unsigned int modifier_ = 0;

      /// If you want to work with tuples (such as a triplet) set the tuplet to something other than 0
      /// This tuplet will modify the final duration with the ratio tuplet:noteValue or tuplet/noteValue
      ///   [Tuplet](https://en.wikipedia.org/wiki/Tuplet)
      unsigned int tuplet_ = 0;

      /// Could potentially use inheritance for these values to overwrite the functions for when we obtain the duration

      /// The duration will use the note value, also known as the relative value, unless otherwise noted. If false, the timeValue will be used for duration.
      bool usingRelativeValue_ = true;
      /// The timeValue will only be used if the usingRelativeValue is set. This will allow for fine tuned control.
      double timeValue_ = 0.0;
   };

   /// The numbers correlate to the midi velocity. Feel free to modify these values as midi velocity can be finicky.
   enum DynamicMarking
   {
      fff = 127,
      ff = 111,
      f = 95,
      mf = 79,
      m = 71,
      mp = 63,
      p = 47,
      pp = 31,
      ppp = 15,

      none = 71, // Default

      Fortississimo = 127,
      Fortissimo = 111,
      Forte = 95,
      MezzoForte = 79,
      Mezzo = 71,
      MezzoPiano = 63,
      Piano = 47,
      Pianissimo = 31,
      Pianississimo = 15
   };

   /// TODO:
   class Dynamic
   {
   public:
   private:
      /// The loudness of the sound. The dynamicMarking corelates to midi velocity
      DynamicMarking dynamicMarking_ = DynamicMarking::none;

      /// TODO: Could potentially use inheritance for these values to overwrite the functions for when we obtain the velocity

      /// If you would like to have control of velocity through the number directly, enable this bool
      bool usingVelocity_ = false;
      /// Velocity is a number between 0 and 127 and correlates directly to the midi velocity
      unsigned int velocity_ = 71;
   };

   /// TODO:
   class Note
   {
   public:
   private:
      /// The pitch will contain all of the frequency information, as well as the note name / theory
      ///   TODO: Should there be an std::optional here? Could make it more difficult to non C++ users. Alternatively could have a bool for rest.
      Pitch pitch_ = Pitch();
      /// The duration will contain all of the note values. The Note Values can be converted to ms/s if given the tempo to calculate.
      Duration duration_ = Duration();
      /// Dynamic will contain an enumerated list of durations using
      Dynamic dynamic_ = Dynamic();
      /// TODO: Add articulation. The idea here is articulation can be things such as staccato, accent, crecendo, vibrato, etc. to a specific note. There can ideally be multiple different articulation modifiers on a single note so this could be a linked list, or it could be an abstract base class with a collection of modifiers, but that could get tricky to modify after the fact. Need to be modifiable after the fact.
      //Articulation articulation_;
      /// TODO: Decide if this is relevant at the note level
      //Timbre timbre_;
   };

   /// TODO: Determine how I can turn this into something functional
   /*enum ChordFunction
{
   I = 1,
   II = 2,
   III = 3,
   IV = 4,
   V = 5,
   VI = 6,
   VII = 7
};*/

   enum Key
   {
      // Major keys
      Cs = 7,
      Fs = 6,
      B = 5,
      E = 4,
      A = 3,
      D = 2,
      G = 1,
      C = 0,
      F = -1,
      Bb = -2,
      Eb = -3,
      Ab = -4,
      Db = -5,
      Gb = -6,
      Cb = -7
      /// TODO: Minor keys???
   };

   enum Interval
   {
      //Specific intervals
      PerfectUnison = 0,
      MinorSecond = 1,
      MajorSecond = 2,
      MinorThird = 3,
      MajorThird = 4,
      PerfectFourth = 5,
      AugmentedFourth = 6,
      DiminishedFifth = 6,
      PerfectFifth = 7,
      MinorSixth = 8,
      MajorSixth = 9,
      MinorSeventh = 10,
      MajorSeventh = 11,
      Octave = 12,

      //Used to build from half steps
      HalfStep = 1,
      WholeStep = 2
   };

   /// TODO: Determine if this is needed? Realistically need an int for the degree, and then the modifier if needed, such as flat or sharp. "#5" for example.
   /*enum ScaleDegree
{
   First = 1,
   Second = 2,
   Third = 3,
   Fourth = 4,
   Fifth = 5,
   Sixth = 6,
   Seventh = 7,
   Eighth = 8,
   Ninth = 9,
   Tenth = 10,
   Eleventh = 11,
   Twelfth = 12,
   Thirteenth = 13,
   Fourteenth = 14,
   Fifteenth = 15,
   Sixteenth = 16
};*/

   /// TODO: How do I represent this?
   ///tonic, supertonic, mediant, subdominant, dominant, submediant, subtonic

   /// TODO: How do I want to do pre determined common scales?
   enum ScaleName
   {
      // Diatonic scales
      Major = 1,
      Minor = 6,

      // Modes
      Ionian = 1,
      Dorian = 2,
      Phrygian = 3,
      Lydian = 4,
      Mixolydian = 5,
      Aeolian = 6,
      Locrian = 7,
      PhrygianDominant = 8
      // ... there's a lot of scales
   };

   /// A scale is a collection of subsequent notes in a series with a predetermined start and end in a specific direction
   /// [Scale](https://en.wikipedia.org/wiki/Scale_(music))
   class Scale
   {
   public:
      Scale() = default;
      Scale(ScaleName name) : sequence_() {}
      ~Scale() = default;

   private:
      /// sequence_: a structure holding a sequence of intervals to the next note such that eventually you will be back at the original starting note. For example for the major scale this will contain; WholeStep, WholeStep, HalfStep, WholeStep, WholeStep, WholeStep, HalfStep
      std::vector<Interval> sequence_;
      /// TODO: ...or will it.. could instead just be a circular buffer of notes
   };

   /// n represents a midi note to obtain a frequency for
   double GetFrequency(int n)
   {
      return (double)(440.0 * 2.0 ^ ((double)(n - 69) / 12.0));
   }

   /// n represents a midi note
   int GetNoteName(int n)
   {
      return n % 12;
   }
   /// n represents a midi note
   int GetOctave(int n)
   {
      return n / 12;
   }

   // Song (tempo, form)
   // |
   // |__ Section (tracks, musical ideas?)
   //     |
   //     |__ Bar (time signature, key, instrument)
   //         |
   //         |__ Note (frequency (name, octave), duration (note duration), volume)

} // tml

#endif // MUSICTHEORYLIBRARY_HPP